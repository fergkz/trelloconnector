<?php

namespace Lib;

class Format{

    public static function formNumber( $string ){
        $nstr = '';
        for( $i = 0; $i < strlen($string); $i++ ){
            $char = substr($string, $i, 1);
            if( strpos("0123456789", $char) !== false ){
                $nstr .= (String)$char;
            }
        }
        return (String)$nstr;
    }

    public static function formCPF( $string ){
        $s = self::formNumber($string);
        $string = substr($s, 0, 3).".".substr($s, 3, 3).".".substr($s, 6, 3)."-".substr($s, 9, 2);
        return (String)$string;
    }

    public static function formNomeProprio( $string ){
        mb_internal_encoding('UTF-8');
        $string = (String)mb_strtolower($string);
        $tmp = explode(" ", $string);
        foreach( $tmp as $t ){
            if( !in_array($t, array( "da", "de", "do", "das", "dos" )) ){
                $tmp2[] = mb_strtoupper(mb_substr($t, 0, 1)).mb_strtolower(mb_substr($t, 1));
            }else{
                $tmp2[] = $t;
            }
        }
        return implode(" ", $tmp2);
    }

    public static function lower( $string ){
        mb_internal_encoding($GLOBALS['config']['ini_set']['default_charset']);
        return (String)mb_strtolower($string);
    }

    public static function agoraDataHoraDb(){
        return date('Y-m-d H:i:s');
    }

    public static function agoraDataHoraDbSomaHoras( $horas ){
        return date('Y-m-d H:i:s', mktime($horas));
    }

    public static function agoraDataDb(){
        return date('Y-m-d');
    }

    public static function agoraDataDbSomaHoras( $horas ){
        return date('Y-m-d', mktime($horas));
    }

    public static function auto( $tempo = null ){
        if( !$tempo ){
            $tempo = "1000";
        }
        echo "<script> window.setInterval('window.location = window.location;', $tempo); </script>";
    }

    public static function somenteNumeros( $string ){
        return preg_replace("/[^0-9]/", "", $string);
    }

    public static function dataToDb( $data ){
        if( !strpos($data, "/") ){
            return $data;
        }
        $data = explode(" ", trim($data));
        $horas = $data[1];
        $data = explode("/", trim($data[0]));
        for( $i = count($data); $i > 0; $i-- ){
            $new[] = $data[$i - 1];
        }
        $new = implode("-", $new);
        return $new.($horas ? " ".$horas : "");
    }

    public static function dataToBr( $data ){
        if( !strpos($data, "-") ){
            return $data;
        }
        $data = explode(" ", trim($data));
        $horas = isset($data[1]) && $data[1] ? $data[1] : null;
        $data = explode("-", trim($data[0]));
        for( $i = count($data); $i > 0; $i-- ){
            $new[] = $data[$i - 1];
        }
        $new = implode("/", $new);
        return $new.($horas ? " ".$horas : "");
    }

    public static function dataDeAte( $dtIni, $dtFim ){
        if( $dtIni && !$dtFim ){
            return "A partir de $dtIni";
        }elseif( !$dtIni && $dtFim ){
            return "Até $dtFim";
        }elseif( $dtIni && $dtFim ){
            return "A partir de $dtIni até $dtFim";
        }else{
            return "Todas as datas";
        }
    }

    public static function subDatas( $data1, $data2, $formatoRetorno ){
        $data1 = trim($data1);
        $data2 = trim($data2);
        if( strpos($data1, "/") ){
            $formatAdoo = true;
            $data1 = dataToDb($data1);
            $data2 = dataToDb($data2);
        }else{
            $formatAdoo = false;
        }

        $data1 = explode(" ", $data1);
        $calendario1 = $data1[0];
        $calendario1 = explode("-", $calendario1);
        $relogio1 = $data1[1];
        $relogio1 = explode(":", $relogio1);

        $ano1 = $calendario1[0];
        $mes1 = $calendario1[1];
        $dia1 = $calendario1[2];
        $hora1 = $relogio1[0];
        $minuto1 = $relogio1[1];
        $segundo1 = $relogio1[2];

        $data2 = explode(" ", $data2);
        $calendario2 = $data2[0];
        $calendario2 = explode("-", $calendario2);
        $relogio2 = $data2[1];
        $relogio2 = explode(":", $relogio2);

        $ano2 = $calendario2[0];
        $mes2 = $calendario2[1];
        $dia2 = $calendario2[2];
        $hora2 = $relogio2[0];
        $minuto2 = $relogio2[1];
        $segundo2 = $relogio2[2];

        $timestamp1 = mktime($hora1, $minuto1, $segundo1, $mes1, $dia1, $ano1);
        $timestamp2 = mktime($hora2, $minuto2, $segundo2, $mes2, $dia2, $ano2);

        $difSegundos = (int)$timestamp1 - (int)$timestamp2;
        if( $difSegundos >= 3600 ){
            $horas = self::lpad(floor($difSegundos / 3600), 0, 2);
            $rest = $difSegundos % 3600;
        }else{
            $horas = self::lpad(0, 0, 2);
            $rest = $difSegundos;
        }
        if( $rest >= 60 ){
            $minutos = self::lpad(floor($rest / 60), 0, 2);
            $segundos = self::lpad($rest % 60, 0, 2);
        }else{
            $minutos = self::lpad(0, 0, 2);
            $segundos = self::lpad($rest, 0, 2);
        }
        $tempoTotal = "{$horas}:{$minutos}:{$segundos}";
        switch( $formatoRetorno ){
            case "s": 
                return (float)$difSegundos;
                break;
            case "i": 
                return (float)$difSegundos / 60;
                break;
            case "H": 
                return (float)$difSegundos / 3600;
                break;
            case "d": 
                return (float)$difSegundos / 86400;
                break;
            case "T": 
                return (String)$tempoTotal;
                break;
            default: 
                return (float)$difSegundos / 86400;
                break;
        }
    }
    
    public static function formataSegundosHoras( $segundos = 0 ){
        $timestamp  = $segundos;
        $horas      = self::lpad(floor($timestamp / 3600), 0, 2);
        $rest       = $timestamp % 3600;
        if( $rest >= 60 ){
            $minutos  = self::lpad(floor($rest / 60), 0, 2);
            $segundos = self::lpad($rest % 60, 0, 2);
        }else{
            $minutos  = self::lpad(0, 0, 2);
            $segundos = self::lpad($rest, 0, 2);
        }
        return "{$horas}:{$minutos}:{$segundos}";
    }

    public static function dataToSec( $data ){
        $data = trim($data);
        if( strpos($data, "/") ){
            $data = dataToDb($data);
        }
        $data = explode(" ", $data);
        $calendario = $data[0];
        $calendario = explode("-", $calendario);
        $relogio = $data[1];
        $relogio = explode(":", $relogio);

        $ano     = (int)$calendario[0];
        $mes     = (int)$calendario[1];
        $dia     = (int)$calendario[2];
        $hora    = (int)$relogio[0];
        $minuto  = (int)$relogio[1];
        $segundo = (int)$relogio[2];

        return (int)mktime($hora, $minuto, $segundo, $mes, $dia, $ano);
    }

    public static function dataToHours( $data ){
        return dataToSec($data) / 3600;
    }

    public static function dataValidaBr( $data ){
        $data = explode("/", "$data"); // fatia a string $dat em pedados, usando / como referência
        $d = $data[0];
        $m = $data[1];
        $y = substr($data[2], 0, 4);
        $res = checkdate($m, $d, $y);
        if( $res == 1 ){
            return true;
        }else{
            return false;
        }
    }

    public static function dataValidaDb( $data ){
        return dataValidaBr(dataToBr($data));
    }

    public static function lpad( $str, $fill, $length ){
        return str_pad($str, $length, $fill, STR_PAD_LEFT);
    }

    public static function rpad( $str, $fill, $length ){
        return str_pad($str, $length, $fill, STR_PAD_RIGHT);
    }

    public static function binToStr( $str, $delimiter = '2' ){
        $str = explode($delimiter, $str);
        $string = '';
        for( $i = 0; $i < count($str); $i++ ){
            $string .= chr(bindec($str[$i]));
        }
        return $string;
    }

    public static function strToBin( $str, $delimiter = '2' ){
        for( $i = 0; $i < strlen($str); $i++ ){
            $n_str[] = base_convert(ord($str[$i]), 10, 2);
        }
        return implode($delimiter, $n_str);
    }

    public static function strToHex( $string ){
        $hex = '';
        for( $i = 0; $i < strlen($string); $i++ ){
            $hex .= dechex(ord($string[$i]));
        }
        return $hex;
    }

    public static function hexToStr( $hex ){
        $string = '';
        for( $i = 0; $i < strlen($hex) - 1; $i+=2 ){
            $string .= chr(hexdec($hex[$i].$hex[$i + 1]));
        }
        return $string;
    }

    public static function strigToUrl( $string ){
        $table = array(
            'Š' => 'S', 'š' => 's', 'Đ' => 'Dj', 'đ' => 'dj', 'Ž' => 'Z',
            'ž' => 'z', 'Č' => 'C', 'č' => 'c', 'Ć' => 'C', 'ć' => 'c',
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A',
            'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
            'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I',
            'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
            'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U',
            'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a',
            'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
            'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i',
            'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o',
            'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u',
            'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b',
            'ÿ' => 'y', 'Ŕ' => 'R', 'ŕ' => 'r',
        );
        $string = strtr($string, $table);
        $string = strtolower($string);
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        $string = preg_replace("/[\s-]+/", " ", $string);
        $string = preg_replace("/[\s_]/", "-", $string);
        return $string;
    }

    public static function converterMinutosEmHoras( $minutos = 0, $separador = ':' ){
        if( $minutos < 0 ){
            $min = abs($minutos);
        }else{
            $min = $minutos;
        }
        $h = floor($min / 60);
        $m = ($min - ($h * 60)) / 100;
        $horas = $h + $m;
        if( $minutos < 0 ){
            $horas *= -1;
        }
        $sep = explode('.', $horas);
        $h = $sep[0];
        if( empty($sep[1]) ){
            $sep[1] = 00;
        }
        $m = $sep[1];
        if( strlen($m) < 2 ){
            $m = $m . 0;
        }
        return sprintf('%02d'.$separador.'%02d', $h, $m);
    }

}