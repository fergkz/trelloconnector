<?php

namespace Lib;

class Load
{
    public static function BugzillaConnector()
    {
        /**
         * Usage: https://code.google.com/p/bugzillaphp/wiki/Usage
         */
        include_once( "bugzillaphp-0.1/BugzillaConnector.php" );
        return "BugzillaConnector";
    }
}