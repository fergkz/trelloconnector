<?php

namespace Model\Sistema;

use Lib\Validate as Validate;
use MVC\Session as Session;
use Model\Trello\Member as Member;

class Usuario extends \System\MyModel
{
    public static $daoTable = "sis_usuario";
    public static $daoPrimary = array('hash' => 'hash');
    public static $daoCols = array(
        'hash'                 => 'hash',
        'nome'                 => 'nome',
        'login'                => 'login',
        'senha'                => 'senha',
        'email'                => 'email',
        'status'               => 'status',
        'trelloMemberId'       => 'trello_member_id',
        'trelloDeveloperKey'   => 'trello_developer_key',
        'trelloAuthToken'      => 'trello_auth_token',
    );
    
    protected $hash;
    protected $nome;
    protected $login;
    protected $senha;
    protected $email;
    protected $status;
    protected $trelloMemberId;
    protected $trelloDeveloperKey;
    protected $trelloAuthToken;
    
    public function getHash()
    {
        return $this->hash;
    }
    public function getLogin()
    {
        return $this->login;
    }
    public function getSenha()
    {
        return $this->senha;
    }
    public function getNome()
    {
        return $this->nome;
    }
    public function getTrelloMemberId()
    {
        return $this->trelloMemberId;
    }
    public function getTrelloDeveloperKey()
    {
        return $this->trelloDeveloperKey;
    }
    public function getTrelloAuthToken()
    {
        return $this->trelloAuthToken;
    }
    public function setHash( $hash )
    {
        $this->hash = $hash;
    }
    public function setLogin( $login )
    {
        $this->login = $login;
    }
    public function setSenha( $senha )
    {
        $this->senha = $senha;
    }
    public function setNome( $nome )
    {
        $this->nome = $nome;
    }
    public function setTrelloMemberId( $trelloMemberId )
    {
        $this->trelloMemberId = $trelloMemberId;
    }
    public function setTrelloDeveloperKey( $trelloDeveloperKey )
    {
        $this->trelloDeveloperKey = $trelloDeveloperKey;
    }
    public function setTrelloAuthToken( $trelloAuthToken )
    {
        $this->trelloAuthToken = $trelloAuthToken;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail( $email )
    {
        $this->email = $email;
    }
    public function getStatus()
    {
        return $this->status;
    }
    public function setStatus( $status )
    {
        $this->status = $status;
    }
    
    public static function getOnline()
    {
        return Session::get("usuario_online");
    }
    
    public function setOnline()
    {
        Session::set("usuario_online", $this);
    }
    
    public function login()
    {
        $lst = self::getList(array(
            "dao.login = ?" => $this->login,
            "(dao.senha = ? or dao.senha = ?)" => array($this->senha, md5($this->senha))
        ), null, 0, 1);
        
        if( $lst['cont_total'] > 0 ){
            $this->hash = $lst['rows'][0]->getHash();
            $this->load();
            $this->setOnline();
            return true;
        }else{
            $this->daoErrorMessage = "Usuário ou senha incorretos";
            return false;
        }
    }
    
    public static function logout()
    {
        Session::destroy();
    }
    
    protected function triggerBeforeSave()
    {
        # Formatação de campos
        $this->login = strtolower(trim($this->login));
        $this->nome  = trim($this->nome);
        $this->senha = trim($this->senha);
        $this->email = trim($this->email);
        $this->trelloDeveloperKey = trim($this->trelloDeveloperKey);
        $this->trelloAuthToken    = trim($this->trelloAuthToken);
        
        if( !$this->status ){
            $this->status = "I";
        }
        
        # Define o hash
        if( $this->daoAction === 'I' ){
            $this->hash = self::geraHashTime();
        }
        
        # Campos obrigatórios
        if( !$this->email ){
            $this->raise("O e-mail deve ser informado");
        }elseif( !Validate::isEmail($this->email) ){
            $this->raise("O e-mail informado é inválido");
        }
        if( !$this->login ){
            $this->raise("O login deve ser informado");
        }
        if( !$this->nome ){
            $this->raise("O nome do usuário deve ser informado");
        }
        if( !$this->senha ){
            $this->raise("A senha deve ser informada");
        }elseif( strlen($this->senha) < 6 ){
            if( strlen($this->old->senha) < 6 ){
                $this->raise("Uma nova senha deve ser informada com tamanho mínimo de 6 caracteres");
            }
            $this->raise("A senha informada deve ter no mínimo 6 caracteres");
        }
        
        # Formatação senha
        if( $this->senha !== $this->old->senha ){
            $this->senha = md5($this->senha);
        }
        
        # Login repetido
        if( $this->daoAction === 'I' || $this->login !== $this->old->login ){
        
            $lst = self::getList(array(
                "dao.login = ?" => $this->login
            ), null, 0, 1);
            if( $lst['cont_total'] > 0 ){
                $this->raise("Já existe um usuário cadastrado com este login");
            }
            
        }
        
        # Importação do Trello
        if( $this->trelloDeveloperKey ){
            
            if( !$this->trelloAuthToken ){
                $this->raise("Para anexar o usuario ao trello, voce deve informar o token de autorização");
            }
            
            $Member = Member::importaMembroByDeveloperToken($this->trelloDeveloperKey, $this->trelloAuthToken);
            
            if( $Member && $Member->getId() ){
                $this->trelloMemberId = $Member->getId();
            }
            
        }
        
    }
    
    
}
