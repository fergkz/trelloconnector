<?php

namespace Model\Sistema;

class Teste extends TestePai
{
    public static $daoTable = "sis_usuario";
    public static $daoPrimary = array('hash' => 'hash');
    public static $daoCols = array(
        'hash'                 => 'hash',
        'nome'                 => 'nome',
        'login'                => 'login',
        'senha'                => 'senha',
        'email'                => 'email',
        'status'               => 'status',
        'trelloMemberId'       => 'trello_member_id',
        'trelloDeveloperKey'   => 'trello_developer_key',
        'trelloAuthToken'      => 'trello_auth_token',
    );
    
    public function show()
    {
        die("in #2");
    }
}
