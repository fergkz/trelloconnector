<?php

namespace Model\Projeto;

use Model\Sistema\Usuario as Usuario;
use MVC\Session as Session;

class Projeto extends \System\MyModel
{
    public static $daoTable = "proj_projeto";
    public static $daoPrimary = array('hash' => 'hash');
    public static $daoCols = array(
        'hash'   => 'hash',
        'titulo' => 'titulo',
        'slug'   => 'slug'
    );
    
    protected $hash;
    protected $titulo;
    protected $slug;
        
    public function getHash()
    {
        return $this->hash;
    }
    public function setHash( $hash )
    {
        $this->hash = $hash;
    }
    public function getTitulo()
    {
        return $this->titulo;
    }
    public function setTitulo( $titulo )
    {
        $this->titulo = $titulo;
    }
    public function getSlug(){
        return $this->slug;
    }
    
    protected function triggerBeforeSave()
    {
        if( $this->daoAction === 'I' ){
            $this->hash = self::geraHashTime(rand(11111,99999), 10);
        }
        
        if( $this->daoAction === 'I' || $this->titulo !== $this->old->titulo ){
            $this->slug = parent::geraSlug($this->titulo);
        }
    }
    
    protected function triggerAfterSave()
    {
        if( $this->daoAction === 'I' ){
            ProjetoUsuario::add( $this->hash, Usuario::getOnline()->getHash() );
        }
    }
    
    public static function getSelecionado()
    {
        return Session::get("projeto_selecionado");
    }
    
    public static function setSelecionado($projetoHash)
    {
        return Session::set("projeto_selecionado", new Projeto($projetoHash));
    }
    
    public static function getListUsuarioOnline()
    {
        $list = ProjetoUsuario::getList(array(
            'dao.usuario_hash = ?' => Usuario::getOnline()->getHash()
        ));
        
        if( $list['cont_total'] > 0 ){
            foreach( $list['rows'] as $Obj ){
                $data[] = $Obj->getProjetoObj();
            }
            return $data;
        }else{
            return null;
        }
    }
    
    public static function listaUsuarios($projetoHash)
    {
        $list = ProjetoUsuario::getList(array(
            'dao.projeto_hash = ?' => $projetoHash
        ));
        
        if( $list['cont_total'] > 0 ){
            foreach( $list['rows'] as $Obj ){
                $data[] = $Obj->getUsuarioObj();
            }
            return $data;
        }else{
            return false;
        }
    }
    
    public function listaCategorias()
    {
        $lista = Categoria::getList(array(
            "dao.projeto_hash = ?" => $this->hash
        ), null, 0, null, array('dao.ordenacao asc'));
        return $lista['rows'];
    }
    
    public static function getBySlug( $slug )
    {
        $lista = self::getList(array(
            'dao.slug = ?' => $slug
        ), null, 0, 1);
        return $lista['cont_total'] > 0 ? $lista['rows'][0] : null;
    }
}
