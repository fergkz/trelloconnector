<?php

namespace Model\Projeto;

use Model\Sistema\Usuario as Usuario;

class Documento extends \System\MyModel
{
    public static $daoTable = "doc_documento";
    public static $daoPrimary = array('hash' => 'hash');
    public static $daoCols = array(
        'hash'              => 'hash',
        'titulo'            => 'titulo',
        'slug'              => 'slug',
        'categoriaHash'     => 'categoria_hash',
        'conteudo'          => 'conteudo',
        'ordenacao'         => 'ordenacao',
        'logData'           => 'log_data',
        'logAction'         => 'log_action',
        'logUsuarioHash'    => 'log_usuario_hash',
    );
    
    protected $hash;
    protected $titulo;
    protected $slug;
    protected $categoriaHash;
    protected $conteudo;
    protected $ordenacao;
    protected $logData;
    protected $logAction;
    protected $logUsuarioHash;

    public function getHash(){
        return $this->hash;
    }
    public function getTitulo(){
        return $this->titulo;
    }
    public function getSlug(){
        return $this->slug;
    }
    public function getCategoriaHash(){
        return $this->categoriaHash;
    }
    public function getConteudo(){
        return $this->conteudo;
    }
    public function getLogData(){
        return $this->logData;
    }
    public function getLogAction(){
        return $this->logAction;
    }
    public function getLogUsuarioHash(){
        return $this->logUsuarioHash;
    }
    public function setHash( $hash ){
        $this->hash = $hash;
    }
    public function setTitulo( $titulo ){
        $this->titulo = $titulo;
    }
    public function setSlug( $slug ){
        $this->slug = $slug;
    }
    public function setCategoriaHash( $categoriaHash ){
        $this->categoriaHash = $categoriaHash;
    }
    public function setConteudo( $conteudo ){
        $this->conteudo = $conteudo;
    }
    public function setLogData( $logData ){
        $this->logData = $logData;
    }
    public function setLogAction( $logAction ){
        $this->logAction = $logAction;
    }
    public function setLogUsuarioHash( $logUsuarioHash ){
        $this->logUsuarioHash = $logUsuarioHash;
    }
    public function getCategoria(){
        return new Categoria($this->categoriaHash);
    }
    public function getOrdenacao(){
        return $this->ordenacao;
    }
    public function setOrdenacao( $ordenacao ){
        $this->ordenacao = $ordenacao;
    }

    protected function triggerBeforeSave()
    {
        if( !$this->titulo ){ $this->raise("O título do documento deve ser informado"); }
        if( !$this->categoriaHash ){ $this->raise("A categoria do documento deve ser informada"); }
        
        if( $this->daoAction === 'I' ){
            $this->hash = self::geraHashTime(rand(11111,99999), 10);
            $this->ordenacao = self::getSeqTime();
        }
        
        if( $this->daoAction === 'I' || $this->titulo !== $this->old->titulo ){
            $this->slug = parent::geraSlug($this->titulo);
        }
        
        if( $this->daoAction === 'U' && !$this->conteudo ){
            $this->raise("O Conteúdo deve ser informado.");
        }
        
        $this->logAction      = $this->daoAction;
        $this->logData        = date("Y/m/d H:i:s");
        $this->logUsuarioHash = Usuario::getOnline()->getHash();
    }
    
//    public static function getList( $whereColumns = array(), $loadAttributes = null, $rowStart = 0, $rowLimit = null, $order = array(), $join = null, $groupBy = null ){
//        $whereColumns[] = "dao.conteudo is not null";
//        return parent::getList($whereColumns, $loadAttributes, $rowStart, $rowLimit, $order, $join, $groupBy);
//    }
}
