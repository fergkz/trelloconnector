<?php

namespace Model\Projeto;

use Model\Sistema\Usuario as Usuario;

class Categoria extends \System\MyModel
{
    public static $daoTable = "doc_categoria";
    public static $daoPrimary = array('hash' => 'hash');
    public static $daoCols = array(
        'hash'              => 'hash',
        'titulo'            => 'titulo',
        'slug'              => 'slug',
        'categoriaPaiHash'  => 'categoria_pai_hash',
        'projetoHash'       => 'projeto_hash',
        'descricao'         => 'descricao',
        'ordenacao'         => 'ordenacao',
        'logData'           => 'log_data',
        'logAction'         => 'log_action',
        'logUsuarioHash'    => 'log_usuario_hash'
    );
    
    protected $hash;
    protected $titulo;
    protected $slug;
    protected $categoriaPaiHash;
    protected $projetoHash;
    protected $descricao;
    protected $ordenacao;
    protected $logData;
    protected $logAction;
    protected $logUsuarioHash;

    public function getHash(){
        return $this->hash;
    }
    public function getTitulo(){
        return $this->titulo;
    }
    public function getSlug(){
        return $this->slug;
    }
    public function getCategoriaPaiHash(){
        return $this->categoriaPaiHash;
    }
    public function getProjetoHash(){
        return $this->projetoHash;
    }
    public function getDescricao(){
        return $this->descricao;
    }
    public function getLogData(){
        return $this->logData;
    }
    public function getLogAction(){
        return $this->logAction;
    }
    public function getLogUsuarioHash(){
        return $this->logUsuarioHash;
    }
    public function setHash( $hash ){
        $this->hash = $hash;
    }
    public function setTitulo( $titulo ){
        $this->titulo = $titulo;
    }
    public function setSlug( $slug ){
        $this->slug = $slug;
    }
    public function setCategoriaPaiHash( $categoriaPaiHash ){
        $this->categoriaPaiHash = $categoriaPaiHash;
    }
    public function setProjetoHash( $projetoHash ){
        $this->projetoHash = $projetoHash;
    }
    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
    }
    public function setLogData( $logData ){
        $this->logData = $logData;
    }
    public function setLogAction( $logAction ){
        $this->logAction = $logAction;
    }
    public function setLogUsuarioHash( $logUsuarioHash ){
        $this->logUsuarioHash = $logUsuarioHash;
    }
    public function getOrdenacao(){
        return $this->ordenacao;
    }
    public function setOrdenacao( $ordenacao ){
        $this->ordenacao = $ordenacao;
    }
    public function getCategoriaPaiObj()
    {
        if( $this->categoriaPaiHash ){
            return new Categoria($this->categoriaPaiHash);
        }
    }
    public function getProjeto($parent = null)
    {
        $Obj = $parent ? $parent : $this;
        
        if( $Obj->getProjetoHash() ){
            return new Projeto( $Obj->getProjetoHash() );
        }else{
            return $this->getProjeto( $Obj->getCategoriaPaiObj() );
        }
    }
    
    private static function getParentTitulo($Categoria, $separador){
        if( $Categoria ){
            $pai = $Categoria->getCategoriaPaiObj();
            $titulo = "";
            if( $pai ){
                $titulo .= self::getParentTitulo($pai, $separador);
            }
            $titulo .= $Categoria->getTitulo().$separador;
            return $titulo;
        }
    }
    public function getTituloComposto($separador = " >> ")
    {
        return self::getParentTitulo($this->getCategoriaPaiObj(), $separador).$this->titulo;
    }

    protected function triggerBeforeSave()
    {
        if( !$this->titulo ){ $this->raise("O título da categoria deve ser informado"); }
        
        if( $this->daoAction === 'I' ){
            $this->hash = self::geraHashTime(rand(11111,99999), 10);
            $this->ordenacao = self::getSeqTime();
        }
        
        if( $this->daoAction === 'I' || $this->titulo !== $this->old->titulo ){
            $this->slug = parent::geraSlug($this->titulo);
        }
        
        $this->logAction      = $this->daoAction;
        $this->logData        = date("Y/m/d H:i:s");
        $this->logUsuarioHash = Usuario::getOnline()->getHash();
        
        if( $this->daoAction === 'D' ){
            $lista = Categoria::getList(array(
                'dao.categoria_pai_hash = ?' => $this->hash
            ), null, 0, 1);
            if( $lista['cont_total'] > 0 ){
                $this->raise("Existem categorias filhas nesta categoria.");
            }
            
            $lista = Documento::getList(array(
                "dao.categoria_hash = ?" => $this->hash,
                "dao.conteudo is null"
            ), null, 0, null, array('dao.ordenacao asc'));
            foreach( $lista['rows'] as $Doc ){
                $Doc->delete();
            }
            
            $lista = Documento::getList(array(
                'dao.categoria_hash = ?' => $this->hash
            ), null, 0, 1);
            if( $lista['cont_total'] > 0 ){
                $this->raise("Existem documentos nesta categoria.");
            }
        }
    }
    
    public function listaDocumentos()
    {
        $lista = Documento::getList(array(
            "dao.categoria_hash = ?" => $this->hash,
            "dao.conteudo is not null"
        ), null, 0, null, array('dao.ordenacao asc'));
        return $lista['rows'];
    }
    
    public function listaCategoriasFilhas()
    {
        $lista = Categoria::getList(array(
            "dao.categoria_pai_hash = ?" => $this->hash
        ), null, 0, null, array('dao.ordenacao asc'));
        return $lista['rows'];
    }
    
    public static function getBySlug( $slug )
    {
        $lista = self::getList(array(
            'dao.slug = ?' => $slug
        ), null, 0, 1);
        return $lista['cont_total'] > 0 ? $lista['rows'][0] : null;
    }
}
