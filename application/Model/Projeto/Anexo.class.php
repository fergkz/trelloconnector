<?php

namespace Model\Projeto;

use Model\Sistema\Usuario as Usuario;

class Anexo extends \System\MyModel
{
    public static $daoTable = "doc_anexo";
    public static $daoPrimary = array('hash' => 'hash');
    public static $daoCols = array(
        'hash'           => 'hash',
        'sequence'       => 'sequence',
        'documentoHash'  => 'documento_hash',
        'titulo'         => 'titulo',
        'relativePath'   => 'relative_path', # sem / no começo e no final
        'relativeUrl'    => 'relative_url',  # sem / no começo e no final
        'fileType'       => 'file_type',     # img/png .. img/jpeg
        'fileSize'       => 'file_size',
        'midiaType'      => 'midia_type',    # IMG .. VID .. AUD
        'logData'        => 'log_data',
        'logAction'      => 'log_action',
        'logUsuarioHash' => 'log_usuario_hash',
    );
    
    protected $hash;
    protected $sequence;
    protected $documentoHash;
    protected $titulo;
    protected $relativePath;
    protected $relativeUrl;
    protected $fileType;
    protected $fileSize;
    protected $midiaType;
    protected $logData;
    protected $logAction;
    protected $logUsuarioHash;
    
    private $tmpPathUpload = null;
    
    public function getHash()
    {
        return $this->hash;
    }
    public function getSequence()
    {
        return $this->sequence;
    }
    public function getDocumentoHash()
    {
        return $this->documentoHash;
    }
    public function getTitulo()
    {
        return $this->titulo;
    }
    public function getRelativePath()
    {
        return $this->relativePath;
    }
    public function getRelativeUrl()
    {
        return $this->relativeUrl;
    }
    public function getFileType()
    {
        return $this->fileType;
    }
    public function getFileSize()
    {
        return $this->fileSize;
    }
    public function getMidiaType()
    {
        return $this->midiaType;
    }
    public function getLogData()
    {
        return $this->logData;
    }
    public function getLogAction()
    {
        return $this->logAction;
    }
    public function getLogUsuarioHash()
    {
        return $this->logUsuarioHash;
    }
    public function setHash( $hash )
    {
        $this->hash = $hash;
    }
    public function setDocumentoHash( $documentoHash )
    {
        $this->documentoHash = $documentoHash;
    }
    public function setTitulo( $titulo )
    {
        $this->titulo = $titulo;
    }
    public function setRelativePath( $relativePath )
    {
        $this->relativePath = $relativePath;
    }
    public function setRelativeUrl( $relativeUrl )
    {
        $this->relativeUrl = $relativeUrl;
    }
    public function setFileType( $fileType )
    {
        $this->fileType = $fileType;
    }
    public function setFileSize( $fileSize )
    {
        $this->fileSize = $fileSize;
    }
    public function setMidiaType( $midiaType )
    {
        $this->midiaType = $midiaType;
    }
    public function setLogData( $logData )
    {
        $this->logData = $logData;
    }
    public function setLogAction( $logAction )
    {
        $this->logAction = $logAction;
    }
    public function setLogUsuarioHash( $logUsuarioHash )
    {
        $this->logUsuarioHash = $logUsuarioHash;
    }
    public function setTmpPathUpload( $tmpPathUpload )
    {
        $this->tmpPathUpload = $tmpPathUpload;
    }

    public function triggerBeforeSave()
    {
        if( $this->daoAction === 'I' ){
            $this->hash = self::geraHashTime(rand(11111,99999), 10);
        }
        
        if( $this->daoAction === 'I' || $this->titulo !== $this->old->titulo ){
            $this->slug = parent::geraSlug($this->titulo);
        }
        
        $this->logAction      = $this->daoAction;
        $this->logData        = date("Y/m/d H:i:s");
        $this->logUsuarioHash = Usuario::getOnline()->getHash();
    }

    public function triggerAfterSave()
    {
        if( !$this->relativePath && $this->tmpPathUpload ){
            $this->load();
            
            $tmpSeq = array();
            for( $i = 0; $i < strlen($this->sequence); $i++ ){
                $tmpSeq[] = (String) $this->sequence[$i];
            }
            
            $path = "media/files/docs/".implode("/", $tmpSeq)."/file";
            
            if( !is_dir(path.ds.$path) ){
                mkdir(path.ds.$path, 0777, true);
                chmod(path.ds.$path, 0777);
            }
            
            move_uploaded_file( $this->tmpPathUpload, path.ds.$path.ds.$this->titulo );
            
            $this->relativeUrl = $path."/".$this->titulo;
            $this->relativePath = $path."/".$this->titulo;

            $this->save();
        }
    }
    
}
