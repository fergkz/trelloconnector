<?php

namespace Model\Projeto;

use Model\Sistema\Usuario as Usuario;

class ProjetoUsuario extends \System\MyModel
{
    public static $daoTable = "proj_projeto_usuario";
    public static $daoPrimary = array('id' => 'id');
    public static $daoCols = array(
        'id' => 'id',
        'usuarioHash' => 'usuario_hash',
        'projetoHash' => 'projeto_hash',
    );
    
    protected $id;
    protected $usuarioHash;
    protected $projetoHash;
    
    public function getId()
    {
        return $this->id;
    }
    public function setId( $id )
    {
        $this->id = $id;
    }
    public function getUsuarioHash()
    {
        return $this->usuarioHash;
    }
    public function setUsuarioHash( $usuarioHash )
    {
        $this->usuarioHash = $usuarioHash;
    }
    public function getProjetoHash()
    {
        return $this->projetoHash;
    }
    public function setProjetoHash( $projetoHash )
    {
        $this->projetoHash = $projetoHash;
    }
    public function getProjetoObj()
    {
        return new Projeto($this->projetoHash);
    }
    public function getUsuarioObj()
    {
        return new Usuario($this->usuarioHash);
    }
    
    protected function triggerBeforeInserUpdate()
    {
        if( $this->daoAction === "U" ){
            $this->raise("Somente inserções e exclusões são permitidas para esta tabela");
        }

        if( $this->daoAction === "I" ){
            $lista = self::getList(array(
                "dao.usuario_hash = ? and dao.projeto_hash = ?" => array($this->usuarioHash, $this->projetoHash)
            ), null, 0, 1);
            if( $lista['cont_total'] > 0 ){
                $this->raise("O usuário já pertence ao projeto selecionado");
            }
        }
    }

    public static function add( $projetoHash, $usuarioHash )
    {
        $ProjUsuario = new ProjetoUsuario();
        $ProjUsuario->setProjetoHash( $projetoHash );
        $ProjUsuario->setUsuarioHash( $usuarioHash );
        
        return $ProjUsuario->save();
    }

    public static function remove( $projetoHash, $usuarioHash )
    {
        $lista = self::getList(array(
            "dao.usuario_hash = ? and dao.projeto_hash = ?" => array($usuarioHash, $projetoHash)
        ), null, 0, 1);
        
        if( $lista['cont_total'] > 0 ){
            return $lista['rows'][0]->delete();
        }
        
        return true;
    }

}
