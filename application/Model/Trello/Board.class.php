<?php

namespace Model\Trello;

use Model\Projeto\Projeto as Projeto;
use Model\Sistema\Usuario as Usuario;

class Board extends TrelloConnector
{
    public static $daoTable = "trello_board";
    public static $daoPrimary = array('id' => 'id');
    public static $daoCols = array(
        'id'   => 'id',
        'key'  => 'url_key',
        'name' => 'name',
        'desc' => 'descricao',
        'url'  => 'url',
        'projetoHash' => 'projeto_hash',
    );
    
    protected $id;
    protected $key;
    protected $name;
    protected $desc;
    protected $url;
    protected $projetoHash;
    
    private $onlineData;
    
    public function setID( $id ){ $this->id = $id; }
    public function getID(){ return $this->id; }
    public function setKey( $key ){ $this->key = $key; }
    public function getKey(){ return $this->key; }
    public function getOnlineData(){ return $this->onlineData; }
    public function getName(){ return $this->name; }
    public function getDesc(){ return $this->desc; }
    public function getUrl(){ return $this->url; }
    
    public function getProjetoHash()
    {
        return $this->projetoHash;
    }
    public function setProjetoHash( $projetoHash )
    {
        $this->projetoHash = $projetoHash;
    }
    public function getProjetoObj()
    {
        return new Projeto($this->projetoHash);
    }
    
    public function atualizaConnectTrello()
    {
        $args = func_get_args();
        $fields = $args[0];
        
        $authToken = Usuario::getOnline()->getTrelloAuthToken();
        $devKey    = Usuario::getOnline()->getTrelloDeveloperKey();
        $boardID  = $this->id ? $this->id : $this->key;
        
        $url = parent::$apiUrl."/1/board/{$boardID}?key={$devKey}&token={$authToken}";
        
        if( in_array("cards", $fields) ){
            $url .= "&cards=all";
        }
        if( in_array("lists", $fields) ){
            $url .= "&lists=all";
        }
        if( in_array("labels", $fields) ){
            $url .= "&labels=all";
        }
        if( in_array("members", $fields) ){
            $url .= "&members=all&member_fields=avatarHash,bio,bioData,confirmed,fullName,idPremOrgsAdmin,initials,memberType,products,status,url,username";
        }
        
        $data = json_decode(file_get_contents($url));
        
        # Atribui dados as variáveis que serão parâmetros
        $dataLists   = @$data->lists;
        $dataCards   = @$data->cards;
        $dataLabels  = @$data->labelNames;
        $dataMembers = @$data->members;
        $dataBoard   = $data;
        
        unset($dataBoard->labelNames);
        unset($dataBoard->cards);
        unset($dataBoard->lists);
        unset($dataBoard->members);
        
        # Atualiza Board
        $this->atualizaDataFromTrello($dataBoard);
        
        # Atualiza Listas
        if( in_array("lists", $fields) ){
            if( $dataLists && count($dataLists) > 0 ){
                foreach( $dataLists as $listData ){
                    $List = new BoardList($listData->id);
                    $List->atualizaDataFromTrello($listData);
                }
            }
        }
        
        # Atualiza Cards
        if( in_array("cards", $fields) ){
            if( $dataCards && count($dataCards) > 0 ){
                foreach( $dataCards as $CardData ){
                    $Card = new Card($CardData->id);
                    $Card->atualizaDataFromTrello($CardData);
                }
            }
        }
        
        # Atualiza Labels
        if( in_array("labels", $fields) && $dataLabels ){
            $dataLabels = (array) $dataLabels;
            foreach( $dataLabels as $color => $name ){
                $Label = new BoardLabel();
                $Label->setBoardId($dataBoard->id);
                $Label->setColor($color);
                $load = $Label->load();
                $Label->setName($name);
                $Label->save( $load ? 'U' : 'I' );
            }
        }
        
        # Atualiza Members
        if( in_array("members", $fields) ){
            foreach( $dataMembers as $MemberData ){
                $Member = new Member($MemberData->id);
                $Member->atualizaDataFromTrello($MemberData);
                $m1[] = "?";
                $m2[] = $MemberData->id;
                
                BoardMember::Connect($dataBoard->id, $MemberData->id);
            }
            $lista = BoardMember::getList(array(
                'dao.board_id = ?' => $dataBoard->id,
                'dao.member_id not in ('.implode(',',$m1).')' => $m2
            ));
            foreach( $lista['rows'] as $Member ){
                $Member->delete();
            }
        }
        
        return true;
    }
    
    private function atualizaDataFromTrello( $dataTrello )
    {
        $this->id   = $dataTrello->id;
        $this->load();
        $this->id   = $dataTrello->id;
        
        $this->key  = end(explode('/', $dataTrello->shortUrl));
        $this->url  = $dataTrello->url;
        $this->name = $dataTrello->name;
        $this->desc = $dataTrello->desc;
        
        return $this->save();
    }
    
    public static function findByKey( $key )
    {
        $lst = self::getList(array(
            'dao.url_key = ?' => $key
        ), null, 0, 1);

        return $lst['cont_total'] > 0 ? $lst['rows'][0] : new Board();
    }
    
    public function getListas()
    {
        $lista = BoardList::getList(array(
            'dao.board_id = ?' => $this->id
        ), null, 0, null, array('dao.pos'));
        
        return $lista['rows'];
    }
    
    public function getMembers()
    {
        $lista = BoardMember::getList(array(
            'dao.board_id = ?' => $this->id
        ));
        
        $members = array();
        foreach( $lista['rows'] as $BoardMember ){
            $members[] = new Member($BoardMember->getMemberId());
        }
        
        return $members;
    }
    
}
