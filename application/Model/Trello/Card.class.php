<?php

namespace Model\Trello;

class Card extends TrelloConnector
{
    public static $daoTable = "trello_card";
    public static $daoPrimary = array('id' => 'id');
    public static $daoCols = array(
        'id'        => 'id',
        'listId'    => 'list_id',
        'name'      => 'name',
        'descricao' => 'descricao',
        'boardID'   => 'idBoard',
        'posicao'   => 'pos',
        'closed'    => 'closed',
        'url'       => 'url'
    );
    
    protected $id;
    protected $listId;
    protected $name;
    protected $descricao;
    protected $boardId;
    protected $posicao;
    protected $closed;
    protected $url;

    public function getListId(){
        return $this->listId;
    }

    public function getName(){
        return $this->name;
    }

    public function getDescricao(){
        return $this->descricao;
    }

    public function getBoardId(){
        return $this->boardId;
    }

    public function getPosicao(){
        return $this->posicao;
    }
    
    public function getClosed(){
        return $this->closed;
    }

    public function getUrl(){
        return $this->url;
    }

    public function setId( $id ){
        $this->id = $id;
        return $this;
    }

    public function setListId( $listId ){
        $this->listId = $listId;
        return $this;
    }

    public function setName( $name ){
        $this->name = $name;
        return $this;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
        return $this;
    }

    public function setBoardId( $boardId ){
        $this->boardId = $boardId;
        return $this;
    }

    public function setPosicao( $posicao ){
        $this->posicao = $posicao;
        return $this;
    }

    public function setClosed( $closed ){
        $this->closed = $closed;
        return $this;
    }

    public function setUrl( $url ){
        $this->url = $url;
        return $this;
    }

    /**
     * Atualiza o registro do card no banco de dados de acordo com o objeto recebido do trello
     * @param Object $dataTrello Objeto recebido pelo trello
     * @return boolean Se conseguiu atualizar o banco de dados de acordo com a classe passada
     */
    public function atualizaDataFromTrello( $dataTrello )
    {
        $this->id   = $dataTrello->id;
        $this->load();
        $this->id   = $dataTrello->id;
        
        $this->listId    = $dataTrello->idList;
        $this->name      = $dataTrello->name;
        $this->descricao = $dataTrello->desc;
        $this->boardID   = $dataTrello->idBoard;
        $this->posicao   = $dataTrello->pos;
        $this->closed    = $dataTrello->closed;
        $this->url       = $dataTrello->url;
        
        return $this->save();
    }
    
    protected function triggerBeforeSave()
    {
        if( $this->daoAction === 'I' && !$this->id ){
            $this->trelloCreate();
        }
        
        
        if( $this->daoAction !== 'D' ){
            $this->closed = $this->closed ? '1' : '0';
        }
    }
 
    private function trelloCreate()
    {
        
        debug($this, 1, "created");
    }
    
    
}