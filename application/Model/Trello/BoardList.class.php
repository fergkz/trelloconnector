<?php

namespace Model\Trello;

class BoardList extends TrelloConnector
{
    public static $daoTable = "trello_board_list";
    public static $daoPrimary = array('id' => 'id');
    public static $daoCols = array(
        'id'      => 'id',
        'boardId' => 'board_id',
        'name'    => 'name',
        'posicao' => 'pos'
    );
    
    protected $id;
    protected $boardId;
    protected $name;
    protected $posicao;
    
    public function setID( $id ){ $this->id = $id; }
    public function getID(){ return $this->id; }
    public function setBoardId( $id ){ $this->boardId = $id; }
    public function getBoardId(){ return $this->boardId; }
    public function setName( $name ){ $this->name = $name; }
    public function getName(){ return $this->name; }
    public function getPosicao(){ return $this->posicao; }
    
    public function __construct( $id = null ){
        parent::__construct($id);
    }
    
    public function atualizaDataFromTrello( $dataTrello )
    {   
        $this->id   = $dataTrello->id;
        $this->load();
        $this->id   = $dataTrello->id;
        
        $this->boardId = $dataTrello->idBoard;
        $this->name    = $dataTrello->name;
        $this->posicao = $dataTrello->pos;
        
        return $this->save();
    }
    
    public function getCards()
    {
        $lista = Card::getList(array(
            'dao.list_id = ?' => $this->id
        ), null, 0, null, array('dao.pos'));
        
        return $lista['rows'];
    }
    
}