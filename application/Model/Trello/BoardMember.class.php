<?php

namespace Model\Trello;

class BoardMember extends TrelloConnector
{
    public static $daoTable = "trello_board_member";
    public static $daoPrimary = array('id' => 'id');
    public static $daoCols = array(
        'id'      => 'id',
        'memberId' => 'member_id',
        'boardId'  => 'board_id'
    );
    
    protected $id;
    protected $memberId;
    protected $boardId;

    public function getId(){
        return $this->id;
    }

    public function getMemberId(){
        return $this->memberId;
    }

    public function getBoardId(){
        return $this->boardId;
    }

    public function setId( $id ){
        $this->id = $id;
    }

    public function setMemberId( $memberId ){
        $this->memberId = $memberId;
    }

    public function setBoardId( $boardId ){
        $this->boardId = $boardId;
    }
    
    public static function Connect( $boardId, $memberId )
    {
        $lst = self::getList(array(
            'dao.board_id = ?' => $boardId,
            'dao.member_id = ?' => $memberId
        ));
        
        if( $lst['cont_total'] <= 0 ){
            $BoardMember = new BoardMember();
            $BoardMember->setBoardId($boardId);
            $BoardMember->setMemberId($memberId);
            $BoardMember->save();
        }
    }
    
}