<?php

namespace Model\Trello;

class Member extends TrelloConnector
{
    public static $daoTable = "trello_member";
    public static $daoPrimary = array('id' => 'id');
    public static $daoCols = array(
        'id'         => 'id',
        'avatarHash' => 'avatar_hash',
        'confirmed'  => 'confirmed',
        'fullName'   => 'full_name',
        'initials'   => 'initials',
        'memberType' => 'member_type',
        'status'     => 'status',
        'url'        => 'url',
        'username'   => 'username',
    );
    
    protected $id;
    protected $avatarHash;
    protected $confirmed;
    protected $fullName;
    protected $initials;
    protected $memberType;
    protected $status;
    protected $url;
    protected $username;

    public function getId(){
        return $this->id;
    }

    public function getAvatarHash(){
        return $this->avatarHash;
    }

    public function getConfirmed(){
        return $this->confirmed;
    }

    public function getFullName(){
        return $this->fullName;
    }

    public function getInitials(){
        return $this->initials;
    }

    public function getStatus(){
        return $this->status;
    }

    public function getUrl(){
        return $this->url;
    }

    public function getUsername(){
        return $this->username;
    }

    public function setId( $id ){
        $this->id = $id;
    }

    public function setAvatarHash( $avatarHash ){
        $this->avatarHash = $avatarHash;
    }

    public function setConfirmed( $confirmed ){
        $this->confirmed = $confirmed;
    }

    public function setFullName( $fullName ){
        $this->fullName = $fullName;
    }

    public function setInitials( $initials ){
        $this->initials = $initials;
    }

    public function setStatus( $status ){
        $this->status = $status;
    }

    public function setUrl( $url ){
        $this->url = $url;
    }

    public function setUsername( $username ){
        $this->username = $username;
    }

    public function getMemberType(){
        return $this->memberType;
    }

    public function setMemberType( $memberType ){
        $this->memberType = $memberType;
    }

    public function getImageUrl( $size = '170' ){
        return "https://trello-avatars.s3.amazonaws.com/{$this->avatarHash}/{$size}.png";
    }

//    public function getImageUrlLocal( $size = '170' ){
//        return url."/media/sistema/members/{$this->avatarHash}/{$size}.png";
//    }
  
    public function atualizaDataFromTrello( $dataTrello )
    {   
        $this->id   = $dataTrello->id;
        $this->load();
        $this->id   = $dataTrello->id;
        
        $this->avatarHash = $dataTrello->avatarHash;
        $this->confirmed  = $dataTrello->confirmed;
        $this->fullName   = $dataTrello->fullName;
        $this->initials   = $dataTrello->initials;
        $this->memberType = $dataTrello->memberType;
        $this->status     = $dataTrello->status;
        $this->url        = $dataTrello->url;
        $this->username   = $dataTrello->username;
        
//        $path = path.ds.'media'.ds.'sistema'.ds.'members'.ds.$this->avatarHash.ds;
//        
//        mkdir($path, 0777, true);
//        
//        file_put_contents( $path.'170.png', file_get_contents($this->getImageUrl('170')) );
//        file_put_contents( $path.'50.png', file_get_contents($this->getImageUrl('50')) );
//        file_put_contents( $path.'30.png', file_get_contents($this->getImageUrl('30')) );
        
        return $this->save();
    }
    
    public static function importaMembroByDeveloperToken( $developerKey, $authToken )
    {
        $url = self::$apiUrl."/1/members/my/?key={$developerKey}&token={$authToken}";
        
//        $retorno = self::sendGet($url);
        $retorno = json_decode(file_get_contents($url));
        
        if( $retorno ){
            $Member = new Member();
            $atualizado = $Member->atualizaDataFromTrello($retorno);
            if( $atualizado ){
                return $Member;
            }
        }
        return false;
    }
    
    public static function getListProjeto( $projetoHash ){
        return self::getList(array(
            "dao.id in 
               ( select bmemb.member_id
                   from trello_board_member bmemb
                  where bmemb.board_id in
                  (select board.id
                     from trello_board board
                    where board.projeto_hash = ?)
                )
            " => $projetoHash
        ));
    }
    
}