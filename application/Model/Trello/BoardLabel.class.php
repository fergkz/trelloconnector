<?php

namespace Model\Trello;

class BoardLabel extends TrelloConnector
{
    public static $daoTable = "trello_board_label";
    public static $daoPrimary = array('id'=>'id');
    public static $daoCols = array(
        'id'      => 'id',
        'boardId' => 'board_id',
        'color'   => 'color',
        'name'    => 'name'
    );
    
    protected $id;
    protected $boardId;
    protected $color;
    protected $name;
    
    public function getId(){
        return $this->id;
    }
    
    public function getBoardId(){
        return $this->boardId;
    }

    public function getColor(){
        return $this->color;
    }

    public function getName(){
        return $this->name;
    }

    public function setBoardId( $boardId ){
        $this->boardId = $boardId;
        return $this;
    }

    public function setColor( $color ){
        $this->color = $color;
        return $this;
    }

    public function setName( $name ){
        $this->name = $name;
        return $this;
    }
    
    public function load( $boardId = null, $color = null ){
        
        if( $boardId && $color ){
            $this->boardId = $boardId;
            $this->color = $color;
        }
        
        $lst = self::getList(array(
            'dao.board_id = ?' => $this->boardId,
            'dao.color = ?' => $this->color
        ), null, 0, 1);
        
        if( $lst['cont_total'] > 0 ){
            $tmp = $lst['rows'][0];
            $this->id      = $tmp->getId();
            $this->boardId = $tmp->getBoardId();
            $this->color   = $tmp->getColor();
            $this->name    = $tmp->getName();
            return true;
        }
        return false;
    }
    
}