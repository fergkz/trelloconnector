<?php

use Model\Projeto\Projeto as Projeto;
use Model\Sistema\Usuario as Usuario;

if( Usuario::getOnline() && !Projeto::getSelecionado() ){
    
    $GLOBALS['request']['mode'] = "painel";
    $GLOBALS['request']['module'] = "index";
    $GLOBALS['request']['submodule'] = "index";
    
    $GLOBALS['page']['lista_projetos'] = Projeto::getListUsuarioOnline();
}