<?php

use Model\Projeto\Projeto as Projeto;
use Model\Projeto\Categoria as Categoria;
use Model\Projeto\Documento as Documento;
use System\MyModel as MyModel;

class DocumentacaoController extends System\MyController
{
    public function visualizacaoAction( $projetoHash, $categoriaHash = null )
    {
        $Projeto = new Projeto($projetoHash);
        $categorias = $Projeto->listaCategorias();
        
        $render['Projeto'] = $Projeto;
        $CategoriaSel = new Categoria($categoriaHash);
        $render['CategoriaSel'] = $CategoriaSel;
        $render['categorias'] = $categorias;
        $render['documentos'] = $CategoriaSel->listaDocumentos();
        
        $this->view("documentacao/visualizacao.twig")->display($render);
    }
    
    public function alteraOrdenacaoDocumentoAction()
    {
        $hashAtual   = $this->post("hash_atual");
        $hashProximo = $this->post("hash_proximo");
        
        $Atual = new Documento($hashAtual);
        
        # Identifica o próximo item
        $Proximo = $hashProximo ? new Documento($hashProximo) : null;
        
        $lista = Documento::getList(array(
            "dao.categoria_hash = ?" => $Atual->getCategoriaHash(),
            "dao.conteudo is not null"
        ), null, 0, null, array("dao.ordenacao asc"));
        
        foreach( $lista['rows'] as $Doc ){
            if( $Doc->getHash() !== $hashAtual ){
                if( $Proximo && $Doc->getOrdenacao() === $Proximo->getOrdenacao() ){
                    $Atual->setOrdenacao(MyModel::getSeqTime());
                    $Atual->save();
                    usleep(100);
                }
                $Doc->setOrdenacao(MyModel::getSeqTime());
                $Doc->save();
            }
            usleep(100);
        }
        if( !$Proximo ){
            $Atual->setOrdenacao(MyModel::getSeqTime());
            $Atual->save();
        };
    }
    
    public function alteraOrdenacaoCategoriaAction()
    {
        $hashAtual   = $this->post("hash_atual");
        $hashProximo = $this->post("hash_proximo");
        
        $Atual = new Categoria($hashAtual);
        
        # Identifica o próximo item
        $Proximo = $hashProximo ? new Categoria($hashProximo) : null;
        
        $lista = Categoria::getList(array(
            "case when dao.projeto_hash is not null 
                then dao.projeto_hash = ? 
             else 
                dao.categoria_pai_hash = ?
             end" => array($Atual->getProjetoHash(), $Atual->getCategoriaPaiHash())
        ), null, 0, null, array("dao.ordenacao asc"));
        
        foreach( $lista['rows'] as $Cat ){
            if( $Cat->getHash() !== $hashAtual ){
                if( $Proximo && $Cat->getOrdenacao() === $Proximo->getOrdenacao() ){
                    $Atual->setOrdenacao(MyModel::getSeqTime());
                    $Atual->save();
                    usleep(100);
                }
                $Cat->setOrdenacao(MyModel::getSeqTime());
                $Cat->save();
            }
            usleep(100);
        }
        if( !$Proximo ){
            $Atual->setOrdenacao(MyModel::getSeqTime());
            $Atual->save();
        };
        
    }
}
