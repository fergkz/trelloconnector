<?php

use Model\Trello\Board as Board;

class BoardCardController extends System\MyController
{   
    public function visualizacaoTabAction( $boardKey = null )
    {
        $Board = Board::findByKey($boardKey);
        $render['Board'] = $Board;
        $this->view("board-card/ajax-lista.twig")->display($render);
    }

    public function atualizaBoardAction()
    {
        $Board = new Board($this->get('boardId'));
        $Board->setId($this->get('boardId'));
        $Board->setKey($this->get('boardKey'));
        
        if( $Board->atualizaConnectTrello($this->get('field')) ){
            $render['status'] = true;
            $this->success("Dados atualizados com sucesso");
        }else{
            $render['status'] = false;
            $render['msg'] = "Falha ao atualizar Trello";
        }
        
        $this->json($render);
    }
    
    public function novoCardAction( $listaId )
    {
//        if( !$this->ajax ){
//            return 404;
//        }
//        $render['lista_id'] = $listaId;
//        $this->view("board/novo-card.twig")->display($render);
    }
    
    public function criarCardAction()
    {
//        if( !$this->ajax ){
//            return 404;
//        }
//        
//        $Card = new Card();
//        $Card->setListId($this->post('lista_id'));
//        $Card->setName($this->post('card_name'));
//        $Card->setDescricao($this->post('card_desc'));
//        
//        if( $Card->save() ){
//            echo "Hangs";
//        }
//        
//        
//        debug($Card);
    }
    
}
