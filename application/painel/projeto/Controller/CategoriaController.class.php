<?php

use Model\Projeto\Projeto as Projeto;
use Model\Projeto\Categoria as Categoria;

class CategoriaController extends System\MyController
{
    public function getOptionsAction( $projetoHash )
    {
        function loadOps( $categorias, $CategoriaPai = null ){
            if( count($categorias) > 0 ){
                foreach( $categorias as $Categoria ){
                    
                    $titulo = $Categoria->getTituloComposto(' > ');
                    
                    echo "<option value=\"{$Categoria->getHash()}\" class='cat-{$Categoria->getHash()}'>{$titulo}</option>";
                    loadOps( $Categoria->listaCategoriasFilhas(), $Categoria );
                }
            }
        }
        
        $Projeto = new Projeto($projetoHash);
        $categorias = $Projeto->listaCategorias();
        
        if( count($categorias) > 0 ){
            loadOps($categorias);
        }else{
            echo "<option value=\"\">Nenhuma categoria cadastrada</option>";
        }
        
    }
    
    public function novaAction( $projetoHash )
    {
        $Projeto = new Projeto($projetoHash);
        
        $render['Projeto'] = $Projeto;

        if( $this->ajax ){
            $this->view("categoria/cadastro-ajax.twig")->display($render);
        }
    }
    
    public function novaSubAction( $categoriaHash )
    {
        $CategoriaPai = new Categoria($categoriaHash);
        
        $render['CategoriaPai'] = $CategoriaPai;

        if( $this->ajax ){
            $this->view("categoria/cadastro-ajax.twig")->display($render);
        }
    }
    
    public function criarAction()
    {   
        $Categoria = new Categoria();
        $Categoria->setProjetoHash( $this->post('projeto','hash') );
        $Categoria->setCategoriaPaiHash( $this->post('categoria','categoria_pai_hash') );
        $Categoria->setDescricao( $this->post('categoria','descricao') );
        $Categoria->setTitulo( $this->post('categoria','titulo') );
        
        _clearErrors();
        if( $Categoria->save() ){
            $render = array(
                'status'           => true,
                'msg'              => "",
                'categoria_hash'   => $Categoria->getHash(),
                'categoria_titulo' => $Categoria->getTitulo()
            );
        }else{
            $render = array(
                'status' => false,
                'msg'    => $Categoria->daoErrorMessage
            );
        }
                
        $this->json($render);
    }
    
    public function edicaoAction( $categoriaHash )
    {
        $Categoria = new Categoria($categoriaHash);
        
        $render['Categoria'] = $Categoria;

        if( $this->ajax ){
            $this->view("categoria/cadastro-ajax.twig")->display($render);
        }
    }
    
    public function atualizarAction()
    {   
        $Categoria = new Categoria( $this->post('categoria','hash') );
        $Categoria->setDescricao( $this->post('categoria','descricao') );
        $Categoria->setTitulo( $this->post('categoria','titulo') );
        
        if( $Categoria->save() ){
            $render = array(
                'status'           => true,
                'msg'              => "",
                'categoria_hash'   => $Categoria->getHash(),
                'categoria_titulo' => $Categoria->getTitulo()
            );
        }else{
            _clearErrors();
            $render = array(
                'status' => false,
                'msg'    => $Categoria->daoErrorMessage
            );
        }
                
        $this->json($render);
    }
    
    public function excluirAction()
    {
        $Categoria = new Categoria($this->post('categoria_hash'));
        if( $Categoria->delete() ){
            $this->json(array(
                'status' => true
            ));
        }else{
            _clearErrors();
            $this->json(array(
                'status' => false,
                'msg'    => $Categoria->daoErrorMessage
            ));
        }
    }
}
