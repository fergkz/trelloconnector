<?php

use Model\Trello\Member as Member;
use Model\Projeto\Projeto as Projeto;

class MemberController extends System\MyController
{
    public function listaAction( $projetoHash )
    {
        $render['Projeto'] = new Projeto($projetoHash);
        $lista = Member::getListProjeto($projetoHash);
        $render['members'] = $lista['rows'];
        $this->view("member/index.twig")->display($render);
    }
    
    public function visualizacaoAction( $memberId  )
    {
        $Member = new Member($memberId);
        
        if( !$Member->getId() ){
            return 404;
        }
        
        $render['Member'] = $Member;
        $this->view("member/visualizacao.twig")->display($render);
    }
}