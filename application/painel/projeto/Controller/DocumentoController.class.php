<?php

use Model\Projeto\Categoria as Categoria;
use Model\Projeto\Documento as Documento;

class DocumentoController extends System\MyController
{
    public function novoAction( $categoriaHash )
    {
        $Categoria = new Categoria($categoriaHash);
        $render['Categoria'] = $Categoria;
        $render['Projeto'] = $Categoria->getProjeto();
        
        $this->limparProjetosInvalidos();
        
        $Documento = new Documento();
        $Documento->setCategoriaHash($categoriaHash);
        $Documento->setTitulo($Categoria->getTitulo()." - ".date('d/m/Y H:i'));
        
        if( $Documento->save() ){
            $this->redirect(url_module."/edicao/".$Documento->getHash());
        }else{
            debug($Documento->daoErrorMessage);
        }
    }
    
    public function edicaoAction( $documentoHash )
    {
        $Documento = new Documento($documentoHash);
        $render['Documento'] = $Documento;
        $Categoria = $Documento->getCategoria();
        $render['Categoria'] = $Categoria;
        $Projeto = $Categoria->getProjeto();
        $render['Projeto'] = $Projeto;
        
        if( $this->ajax ){
            $this->view("documento/cadastro-ajax.twig")->display($render);
        }else{
            $this->view("documento/cadastro.twig")->display($render);
        }
    }
    
    public function alterarAction()
    {
        $Documento = new Documento( $this->post('documento','hash')     );
        $Documento->setTitulo(      $this->post('documento','titulo')   );
        $Documento->setConteudo(    $this->post('documento','conteudo') );
        
        if( $Documento->save() ){
            $this->json(array(
                'status' => true
            ));
        }else{
            _clearErrors();
            $this->json(array(
                'status' => false,
                'msg'    => $Documento->daoErrorMessage
            ));
        }
    }
    
    public function listaAction( $categoriaHash )
    {
        $Categoria = new Categoria($categoriaHash);
        $render['Categoria'] = $Categoria;
        
        if( $this->ajax ){
            $this->view("documento/lista-ajax.twig")->display($render);
        }
    }
    
    private function limparProjetosInvalidos()
    {
        $lista = Documento::getList(array(
            'dao.conteudo is null',
            "TIME_TO_SEC(TIMEDIFF(?, dao.log_data)) > 1800" => date("Y-m-d H:i:s") // 30 minutos
        ));
        
        foreach( $lista['rows'] as $Obj ){
            $Obj->save('D');
        }
    }
    
    public function testeAction(){ $this->view("documento/teste.twig")->display(); }
}
