<?php

use Model\Projeto\Projeto as Projeto;
use Model\Projeto\Categoria as Categoria;

class DocumentacaoOldController extends System\MyController
{
    public function visualizacaoAction( $projetoHash, $categoriaHash = null )
    {
        $Projeto = new Projeto($projetoHash);
        $categorias = $Projeto->listaCategorias();
        
        $render['Projeto'] = $Projeto;
        $CategoriaSel = new Categoria($categoriaHash);
        $render['CategoriaSel'] = $CategoriaSel;
        $render['categorias'] = $categorias;
        $render['documentos'] = $CategoriaSel->listaDocumentos();

        function intraloadOps( $categorias, $CategoriaPai = null ){
            if( count($categorias) > 0 ){
                foreach( $categorias as $Categoria ){
                    
                    $titulo = $Categoria->getTituloComposto(' > ');
                    $tmp = array();
                    $xexpT = explode(" > ", $titulo);
                    foreach( $xexpT as $row ){
                        $tmp[] = ( $row !== end($xexpT) ? "" : $Categoria->getTitulo() );
                    }
                    $titulo = implode("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $tmp);
                    
                    echo "<option value=\"{$Categoria->getHash()}\" class='cat-{$Categoria->getHash()}'>{$titulo}</option>";
                    intraloadOps( $Categoria->listaCategoriasFilhas(), $Categoria );
                }
            }
        }
        
        $this->view()->declareFunction("intraloadOps")->setTemplate("documentacao-old/visualizacao.twig")->display($render);
    }
}
