<?php

use Model\Trello\Board as Board;

class BoardMembroController extends System\MyController
{   
    public function visualizacaoTabAction( $boardKey = null )
    {
        $Board = Board::findByKey($boardKey);
        $render['Board'] = $Board;
        $this->view("board-membro/ajax-lista.twig")->display($render);
    }
}
