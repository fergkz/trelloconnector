<?php

use Model\Trello\Board as Board;
use Model\Projeto\Projeto as Projeto;
use Model\Sistema\Usuario as Usuario;

class BoardController extends System\MyController
{
    public function __construct()
    {
        parent::__construct();
        if( !Usuario::getOnline()->getTrelloDeveloperKey() ){
            $this->view("board/nao-autorizado-trello.twig")->display();
            exit;
        }
    }
    
    public function listaAction( $projetoHash = null )
    {
        $render['lista_boards'] = Board::getList(array(
            "dao.projeto_hash = ?" => $projetoHash
        ));
        $render['Projeto'] = new Projeto($projetoHash);
        $this->view("board/index.twig")->display($render);
    }
    
    public function visualizacaoAction( $boardKey = null )
    {
        $Board = Board::findByKey($boardKey);
        
        if( !$Board->getID() ){
            $Board->getConnectTrelo($boardKey);
            
            if( !$Board->getOnlineData() ){
                $this->error("O ID do board informado é inválido");
                $this->redirect(url_module."/lista/".Projeto::getSelecionado()->getHash());
            }
        }
        
        $render['Board'] = $Board;
        $this->view("board/visualizacao.twig")->display($render);
    }

    public function atualizaBoardAction()
    {
        $Board = new Board($this->get('boardId'));
        $Board->setId($this->get('boardId'));
        $Board->setKey($this->get('boardKey'));
        
        if( $Board->atualizaConnectTrello($this->get('field')) ){
            $render['status'] = true;
            $this->success("Dados atualizados com sucesso");
        }else{
            $render['status'] = false;
            $render['msg'] = "Falha ao atualizar Trello";
        }
        
        $this->json($render);
    }
    
    public function novoAction( $projetoHash )
    {
        $render['Projeto'] = new Projeto($projetoHash);
        $this->view("board/cadastro.twig")->display($render);
    }
    
    public function buscarTrelloAction()
    {
        $lista = Board::getList(array(
            "dao.url_key = ?" => $this->post('id')
        ), null, 0, 1);
        if( $lista['cont_total'] > 0 ){
            $this->error("O board selecionado já existe no sistema");
            $this->redirect(url_module."/novo/".$this->post('projeto_hash'));
        }
        
        $Board = new Board();
        $Board->setProjetoHash($this->post('projeto_hash'));
        $Board->setKey($this->post('id'));
        
        if( $Board->atualizaConnectTrello(array('cards','lists','labels','members')) ){
            $this->redirect(url_module."/visualizacao/{$Board->getKey()}");
        }else{
            $this->error("O ID do board informado é inválido");
            $this->redirect(url_module."/novo/".$this->post('projeto_hash'));
        }
    }
}
