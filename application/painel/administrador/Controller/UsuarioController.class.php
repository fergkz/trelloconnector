<?php

use MVC\Session as Session;
use Model\Sistema\Usuario as Usuario;

class UsuarioController extends System\MyController
{
    public function listaAction()
    {
        $this->view("usuario/lista.twig")->display();
    }
    
    public function getListaUsuariosAjaxAction()
    {
        $render['usuarios'] = Usuario::getList();
        $this->view("usuario/ajax-lista.twig")->display($render);
    }
    
    public function novoAction()
    {
        $this->view("usuario/ajax-cadastro.twig")->display();
    }
    
    public function edicaoAction( $usuarioHash )
    {
        $render['Usuario'] = new Usuario($usuarioHash);
        $this->view("usuario/ajax-cadastro.twig")->display($render);
    }
    
    public function criarAction()
    {
        $Usuario = new Usuario();
        $Usuario->setLogin(  $this->post('login')  );
        $Usuario->setNome(   $this->post('nome') );
        $Usuario->setSenha(  $this->post('senha') );
        $Usuario->setEmail(  $this->post('email') );
        if( $Usuario->save() ){
            $this->json(array(
                'status' => true
            ));
        }else{
            _clearErrors();
            $this->json(array(
                'status' => false,
                'msg'    => $Usuario->daoErrorMessage
            ));
        }
    }
    
    public function atualizarAction()
    {
        $Usuario = new Usuario($this->post('hash'));
        $Usuario->setLogin(  $this->post('login')  );
        $Usuario->setNome(   $this->post('nome') );
        if( $this->post('senha') ){
            $Usuario->setSenha(  $this->post('senha') );
        }
        $Usuario->setEmail(  $this->post('email') );
        if( $Usuario->save() ){
            $this->json(array(
                'status' => true
            ));
        }else{
            _clearErrors();
            $this->json(array(
                'status' => false,
                'msg'    => $Usuario->daoErrorMessage
            ));
        }
    }
    
}
