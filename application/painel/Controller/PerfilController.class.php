<?php

use Model\Sistema\Usuario as Usuario;

class PerfilController extends System\MyController
{   
    public function indexAction()
    {
        $render['Usuario'] = Usuario::getOnline();
        $this->view("perfil/visualizacao.twig")->display($render);
    }
    
    public function criarAction()
    {
        $Projeto = new Projeto();
        $Projeto->setTitulo($this->post('titulo'));

        if( $Projeto->save() ){
            _setSuccess("Projeto criado com sucesso");
            $this->json(array(
                'status'   => true,
                'hash'     => $Projeto->getHash(),
                'redirect' => url_module."/seleciona/".$Projeto->getHash()
            ));
        }else{
            _clearErrors();
            $this->json(array(
                'status' => false,
                'msg'    => $Projeto->daoErrorMessage
            ));
        }
    }
    
    public function edicaoAction()
    {
        $this->view("perfil/cadastro.twig")->display();
    }
    
    public function alterarAction()
    {   
        $Usuario = Usuario::getOnline();
        $Usuario->load();
        
        $Usuario->setLogin(  $this->post('login')  );
        $Usuario->setNome(   $this->post('nome') );
        if( $this->post('senha') ){
            if( $this->post('senha') !== $this->post('rsenha') ){
                $this->json(array(
                    'status' => false,
                    'msg'    => "As senhas informadas não conferem"
                ));
            }
            $Usuario->setSenha(  $this->post('senha') );
        }
        $Usuario->setEmail(  $this->post('email') );
        
        if( $this->post('trello_developer_key') ){
            $Usuario->setTrelloDeveloperKey( $this->post('trello_developer_key') );
        }
        
        if( $this->post('trello_auth_token') ){
            $Usuario->setTrelloAuthToken( $this->post('trello_auth_token') );
        }
        
        if( $Usuario->save() ){
            $Usuario->setOnline(); 
            $this->json(array(
                'status'   => true,
                'redirect' => url."/painel/perfil"
            ));
        }else{
            _clearErrors();
            $this->json(array(
                'status' => false,
                'msg'    => $Usuario->daoErrorMessage
            ));
        }
    }    
}
