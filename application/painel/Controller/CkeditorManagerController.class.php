<?php

use Model\Projeto\Anexo as Anexo;

class CkeditorManagerController extends System\MyController
{
    public function uploadImagesAction()
    {   
        $file = $this->files('upload');
        
        $Anexo = new Anexo();
        $Anexo->setFileType(  $file['type']);
        $Anexo->setFileSize(  $file['size']);
        $Anexo->setMidiaType( "IMG");
        $Anexo->setTitulo(    $file['name']);
        $Anexo->setDocumentoHash( $this->get('documento') );
        
        $Anexo->setTmpPathUpload($file['tmp_name']);
        
        _clearErrors();
        if( $Anexo->save() ){
            $filename = url."/".$Anexo->getRelativeUrl();
            
            echo <<<EOF
<script type="text/javascript">
(function(){var d=document.domain;while (true){try{var A=window.parent.document.domain;break;}catch(e) {};d=d.replace(/.*?(?:\.|$)/,'');if (d.length==0) break;try{document.domain=d;}catch (e){break;}}})();
EOF;
 
 
  $rpl = array( '\\' => '\\\\', '"' => '\\"' ) ;
  echo "window.parent.CKEDITOR.tools.callFunction(\"{$this->get('CKEditorFuncNum')}\",\"{$filename}\");";
 
  echo '</script>';

//            echo "<script>";
//            echo "window.opener.CKEDITOR.tools.callFunction('{$this->get('CKEditorFuncNum')}', '{$filename}' );";
//            echo "window.close();";
//            echo "</script>";
        }else{
            //echo "<input id=\"cke_101_fileInput_input\" aria-labelledby=\"cke_100_label\" type=\"file\" name=\"upload\" size=\"38\">";   
            echo "<div style='color:red;'>{$Anexo->daoErrorMessage}</div>";   
        }
        
    }
    
}