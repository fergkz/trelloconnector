<?php

use Model\Projeto\Projeto as Projeto;
use Model\Projeto\ProjetoUsuario as ProjetoUsuario;
use Model\Sistema\Usuario as Usuario;

class ProjetoUsuarioController extends System\MyController
{   
    public function visualizacaoTabAction( $projetoHash )
    {
        $render['Projeto'] = new Projeto($projetoHash);
        $this->view("projeto-usuario/tab-content.twig")->display($render);
    }

    public function listaAjaxAction( $projetoHash )
    {
        $render['usuarios'] = Projeto::listaUsuarios($projetoHash);
        $render['Projeto'] = new Projeto($projetoHash);
        $this->view("projeto-usuario/ajax-lista.twig")->display($render);
    }
    
    public function adicionaUsuarioAction( $projetoHash )
    {
        $render['Projeto'] = new Projeto($projetoHash);
        
        $render['lista_usuarios_disponiveis'] = Usuario::getList(array(
            "dao.hash not in (select tmp.usuario_hash from proj_projeto_usuario tmp where tmp.projeto_hash = ?)" => $projetoHash
        ));
        
        $this->view("projeto-usuario/adicionar-usuario.twig")->display($render);
    }
    
    
    public function anexarUsuarioProjetoAction()
    {
        ProjetoUsuario::add( $this->post('projeto_hash'), $this->post('usuario_hash') );
        $this->json(array(
            'status' => true
        ));
    }
    
    public function desanexarUsuarioProjetoAction()
    {
        if( $this->post('usuario_hash') === Usuario::getOnline()->getHash() ){
            $this->json(array(
                'status' => false,
                'msg'    => "Não é possível excluir o seu próprio usuário"
            ));
        }
        
        ProjetoUsuario::remove( $this->post('projeto_hash'), $this->post('usuario_hash') );
        $this->json(array(
            'status' => true
        ));
    }
    
}
