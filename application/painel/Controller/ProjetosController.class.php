<?php

use Model\Projeto\Projeto as Projeto;

class ProjetosController extends System\MyController
{   
    public function selecionaAction( $projetoHash )
    {
        Projeto::setSelecionado( $projetoHash );
        $this->redirect( url_module."/visualizacao/{$projetoHash}" );
    }
    
    public function visualizacaoAction( $projetoHash )
    {
        $render['Projeto'] = new Projeto($projetoHash);
        $this->view("projetos/visualizacao.twig")->display($render);
    }
    
    public function novoAction()
    {
        $this->view("projetos/cadastro.twig")->display();
    }
    
    public function criarAction()
    {
        $Projeto = new Projeto();
        $Projeto->setTitulo($this->post('titulo'));

        if( $Projeto->save() ){
            _setSuccess("Projeto criado com sucesso");
            $this->json(array(
                'status'   => true,
                'hash'     => $Projeto->getHash(),
                'redirect' => url_module."/seleciona/".$Projeto->getHash()
            ));
        }else{
            _clearErrors();
            $this->json(array(
                'status' => false,
                'msg'    => $Projeto->daoErrorMessage
            ));
        }
    }
    
    public function edicaoAction( $projetoHash )
    {
        $render['Projeto'] = new Projeto($projetoHash);
        $this->view("projetos/cadastro.twig")->display($render);
    }
    
    public function alterarAction()
    {
        $Projeto = new Projeto( $this->post('hash') );
        
        if( !$Projeto || !$Projeto->getHash() || !$this->post('hash') ){
            $this->json(array(
                'status' => false,
                'msg'    => "O projeto informado é inválido"
            ));
        }

        $Projeto->setTitulo($this->post('titulo'));

        if( $Projeto->save() ){
            _setSuccess("Projeto atualizado com sucesso");
            $this->json(array(
                'status'   => true,
                'hash'     => $Projeto->getHash(),
                'redirect' => url_module."/visualizacao/".$Projeto->getHash()
            ));
        }else{
            _clearErrors();
            $this->json(array(
                'status' => false,
                'msg'    => $Projeto->daoErrorMessage
            ));
        }
    }    
}
