<?php

class IndexController extends System\MyController
{
    public function indexAction()
    {
        $this->view("index/index.twig")->display();
    }

    public function index2Action()
    {
        $boardID = trello_gp_homolog_boarder_key;
        $appID = trello_gp_homolog_application_key;
        $appToken = trello_gp_homolog_application_token;
        $url = "https://api.trello.com/1/board/{$boardID}?key={$appID}&token={$appToken}";
        
//        $contentJson = file_get_contents($url);
//        debug(json_decode($contentJson), 1, 'DATA BOARD' );
        
//        $url2 = "https://trello.com/1/members/me?key={$appID}&token={$appToken}";
//        $content2 = json_decode(file_get_contents($url2));
//        
//        debug($content2, 1, 'MEMBER' );
        /*
        $i = 0;
        foreach( $content2->idBoards as $row ){
            $i++;
            $url = "https://api.trello.com/1/board/{$row}?key={$appID}&token={$appToken}";
            debug(json_decode(file_get_contents($url)), 1, "Board {$i}");
        }
        */
        
        
//        $url2 = "https://trello.com/1/members/my/cards?key={$appID}&token={$appToken}";
//        $content2 = json_decode(file_get_contents($url2));
//        
//        debug($content2, 1, 'CARDS of APP' );
        
        
//        $url2 = "https://trello.com/1/members/my/organizations?key={$appID}&token={$appToken}";
//        $content2 = json_decode(file_get_contents($url2));
//        
//        debug($content2, 1, 'ORGANIZATIONS' );
//        
        
        $url2 = "https://trello.com/1/boards/{$boardID}?key={$appID}&token={$appToken}&lists=all&cards=visible";
        $content2 = json_decode(file_get_contents($url2));
        
        foreach( $content2->lists as $lista){
            
            echo "<div style='border:1px solid red;padding:4px;margin:5px;'>";
            
            echo "<h1>{$lista->id} - {$lista->name}</h1>";
            
            foreach( $content2->cards as $card ){
                if( $card->idList == $lista->id ){
                    echo "<p>";
                    echo "<b>{$card->id}</b> {$card->name}";
                    echo "</p>";
                }
            }
            
            echo "</div>";
            
        }
        
        debug($content2, 1, 'BOARD' );
    }
    
}