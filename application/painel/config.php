<?php

use Model\Sistema\Usuario as Usuario;
use Model\Projeto\Projeto as Projeto;

if( !Usuario::getOnline() ){
    
    $GLOBALS['request']['mode'] = "f";
    $GLOBALS['request']['module'] = "sessao";
    $GLOBALS['request']['submodule'] = "login";
    
    _setError("Você deve estar logado para acessar o painel de controle");
    
}else{

    if( Projeto::getSelecionado() ){
        $GLOBALS['page']['projeto_atual'] = Projeto::getSelecionado();
    }
    $GLOBALS['page']['lista_projetos'] = Projeto::getListUsuarioOnline();

}