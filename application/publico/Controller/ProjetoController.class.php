<?php

use Model\Projeto\Projeto as Projeto;
use Model\Projeto\Categoria as Categoria;

class ProjetoController extends System\MyController
{   
    public function visualizacaoAction( $projetoSlug = null, $categoriaSlug = null )
    {
        $render = array();
        $Projeto = Projeto::getBySlug($projetoSlug);
        $GLOBALS['page']['projeto_atual'] = $Projeto;
        $render['Projeto'] = $Projeto;
        $categorias = $Projeto->listaCategorias();
        $render['categorias'] = $categorias;
        $render['Categoria'] = $categoriaSlug ? Categoria::getBySlug($categoriaSlug) : null;
        
        $this->view('projeto/visualizacao.twig')->display($render);
    }
    
}