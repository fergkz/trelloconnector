<?php

use Model\Sistema\Usuario as Usuario;

class SessaoController extends System\MyController
{   
    public function loginAction()
    {
        $render = array();
        if( _getErrors() ){
            $render['error_msg'] = implode("<br/>", _getErrors());
        }
        $this->view('sessao/login.twig')->display($render);
    }
    
    public function entrarAction()
    {
        $Usuario = new Usuario();
        $Usuario->setLogin($this->post('login'));
        $Usuario->setSenha($this->post('senha'));
        
        if( $Usuario->login() ){
            $this->json(array(
                "status" => true
            ));
        }else{
            $this->json(array(
                "status" => false,
                "msg" => $Usuario->daoErrorMessage
            ));
        }
    }
    
    public function sairAction()
    {
        Usuario::logout();
        $this->redirect(url);
    }
}