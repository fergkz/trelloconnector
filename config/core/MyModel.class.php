<?php

namespace System;

use Core\System\Functions as Functions;
use Lib\Format as Format;

class MyModel
{

    public function __construct( $id = null )
    {
        if( $id ){
            foreach (static::$daoPrimary as $attribute => $column) {
                $this->$attribute = $id;
            }
        
            return $this->load();
        }
    }

    protected function raise( $message )
    {
        return _raise( $message );
    }
    
    public function clearParse()
    {
        unset($this->_parse);
        unset($this->_pk);
        unset($this->_action);
    }
    
    public function load ($attributes = null)
    {
        $db = new \Core\System\DataBaseConnection();
        foreach (static::$daoPrimary as $attribute => $column) {
            if( empty($this->$attribute) ){
                return false;
            }
            $where[] = $column." = ?";
            $bind[] = $this->$attribute;
        }

        $sql = "SELECT * FROM ".static::$daoTable." WHERE ".implode(" and ", $where);

        $res = $db->query($sql, $bind);
        if( !$res || count($res) < 1 ){
            foreach (static::$daoPrimary as $attribute => $column) {
                $this->{$attribute} = null;
            }
            return false;
        }
        
        $row = $res[0];
        
        foreach (static::$daoCols as $attribute => $column) {
            
            if (!$attributes || in_array($attribute, $attributes)) {
                $this->{$attribute} = $row[$column];
            }
        }

        $this->clearParse();
        
        if( method_exists($this, 'triggerAfterLoad') ){
            $this->triggerAfterLoad();
        }
        
        return $this;
    }

    public function delete ()
    {
        return $this->save('D');
    }

    private function loadQueryParams($wherePrimary, $bindPrimary)
    {
        $primaryExists = $wherePrimary && count($wherePrimary) > 0 ? true : false;
        if ($this->daoAction !== 'D') {
            if( $primaryExists || $this->daoAction === 'U' ){
                // update
                $this->daoAction = "U";
                foreach (static::$daoCols as $attribute => $column) {
                    $bind[] = $this->$attribute;
                    $this->$attribute = $this->$attribute;
                    $sqlCols[] = " {$column} = ? ";
                }
                $sql = "update ".static::$daoTable." set ".implode(", ", $sqlCols)." where ".implode(' and ', $wherePrimary);
                $bind = array_merge($bind, $bindPrimary);
            }else{
                // insert
                $this->daoAction = "I";
                foreach( static::$daoCols as $attribute => $column ){
                    $this->$attribute = @$this->$attribute;
                    $bind[] = @$this->$attribute;
                    $sqlCols[] = $column;
                    $sqlVals[] = "?";
                }
                $sql = "insert into ".static::$daoTable." (".implode(", ", $sqlCols).") values (".implode(" ,", $sqlVals).")";
            }
        }else {
            // delete
            if (empty($wherePrimary)) {
                return false;
            }
            $sql = "delete from ".static::$daoTable." where ".implode(" and ", $wherePrimary);
            $bind = $bindPrimary;
        }
//        debug($sql,1); debug($bind);
        return array(
            'sql' => $sql,
            'bind'=> $bind
        );
    }
    
    public function save ($operacao = null)
    {
        $this->old = clone $this;
        $this->old->load();
        
        $db = new \Core\System\DataBaseConnection();
        $primaryExists = true;
        $primaryAttributes = null;
        
        try{
            if( empty($this->old) ){
                $this->old = clone $this;
                $this->old->load();
                unset($this->old->old);
            }
            
            foreach (static::$daoPrimary as $attribute => $column) {
                if( empty($this->old->$attribute) ){
                    $primaryExists = false;
                }else{
                    $wherePrimary[] = $column." = ?";
                    $bindPrimary[] = $this->$attribute;
                }
                $primaryAttributes[] = $attribute;
            }

            $this->daoAction = $operacao;
            
            $this->loadQueryParams(@$wherePrimary, @$bindPrimary);
            
            /** Begin::Triggers **/
            if( method_exists($this, 'triggerBeforeSave') ){
                $this->triggerBeforeSave();
            }
            /** End::Triggers **/
            
            $tmp  = $this->loadQueryParams(@$wherePrimary, @$bindPrimary);
            $sql  = $tmp['sql'];
            $bind = $tmp['bind'];

            $res = $db->execute($sql, $bind);

            if ($res) {
                if( $this->daoAction === 'I' ){
                    $primary = is_array($primaryAttributes) ? $primaryAttributes[0] : $primaryAttributes;
                    if( $primary && !$this->$primary ){
                        $this->$primary = $db->lastId();
                    }
                }
                /** Begin::Triggers **/
                if( method_exists($this, 'triggerAfterSave') ){
                    $this->triggerAfterSave();
                }
                /** End::Triggers **/
                
                return $this;
            }else{
                $this->daoErrorMessage = implode('',_getErrors());
                return false;
            }
        }catch( \Exception $e ){
            $db->rollback();
            $this->daoErrorMessage = $e->getMessage();
            _setError($e->getMessage());
            return false;
        }
    }

    public static function getList ($whereColumns = array (), $loadAttributes = null, $rowStart = 0, $rowLimit = null, $order = array (), $join = null, $groupBy = null)
    {
        $db = new \Core\System\DataBaseConnection();
        $where = $bind = array ();

        if( $whereColumns ){
            foreach( $whereColumns as $column => $value ){

                if( !in_array((String)$column, static::$daoCols) ){

                    if( !is_array($value) ){
                        if( !is_numeric($column) ){
                            $bind[] = $value;
                        }else{
                            $column = $value;
                        }
                    }else {
                        $arrayVals = array();
                        foreach( $value as $val ){
                            $bind[] = $val;
                        }
                    }

                    $where[] = $column;

                }else{

                    if (!is_array($value)) {
                        $bind[] = $value;
                        $where[] = "{$column} like ?";
                    }else {
                        $arrayVals = array ();
                        foreach ($value as $val) {
                            $arrayVals[] = "?";
                            $bind[] = $val;
                        }
                        $vals = implode(",", $arrayVals);
                        $where[] = "{$column} in ({$vals})";
                    }

                }

            }
        }

        if ($loadAttributes && is_array($loadAttributes) && count($loadAttributes) > 0) {
            foreach( $loadAttributes as $attribute ){
                if( stristr($attribute, ' as ') ){
                    $tmp = explode( ' ', trim($attribute) );
                    
                    $attribute = array_pop($tmp);
                    array_pop($tmp);
                    
                    $tColumns = trim(implode(' ', $tmp));
                    
                    $selectCols[] = "{$tColumns} as ".static::$daoCols[$attribute];
                    
                }else{
                    $selectCols[] = "dao.".static::$daoCols[$attribute]." as ".static::$daoCols[$attribute];
                }
            }
            $selectCols = implode(", ", $selectCols);
        }else {
            $selectCols = "dao.*";
        }

        $sql = "SELECT {$selectCols} FROM ".static::$daoTable." dao";
        
        if( $join ){
            $sql .= " {$join} ";
        }
        
        if ($where && count($where) > 0) {
            $sql .= " where ".implode(" and ", $where);
        }
        
        $resCont = $db->query("select count(*) as cont_total from ({$sql}) t", $bind);
        $result['cont_total'] = $resCont[0]['cont_total'];

        if ($order && count($order) > 0) {
            $sql .= " order by ".implode(", ", $order);
        }

        if ($groupBy && count($groupBy) > 0) {
            $sql .= " group by ".implode(", ", $groupBy);
        }

        if ($rowLimit) {
            $sql .= " limit {$rowStart}, {$rowLimit}";
        }
        
        $res = $db->query($sql, $bind);
        
        $result['sql'] = $sql;
        $result['bind'] = $bind;

        if ($res) {
            $dados = null;
            foreach ($res as $row) {
                $classname = get_called_class();
                $Obj = new $classname;

                foreach ($row as $column => $value) {
                    if( !$column || !in_array($column, static::$daoCols) ){
                        continue;
                    }
                    $tmp = array_keys(static::$daoCols, $column);
                    $key = $tmp[0];
                    $Obj->$key = $value;
                }

                $Obj->clearParse();
                
                if( method_exists($Obj, 'triggerAfterLoad') ){
                    $Obj->triggerAfterLoad();
                }
        
                $dados[] = $Obj;
            }

            $result['rows'] = $dados;

        }else {
            $result['rows'] = array();
        }
        return $result;
    }
    
    public function __call( $method, $params )
    {
        $attr = @strtolower(substr($method, 3, 1)).substr($method, 4);
        
        if( !property_exists($this, $attr) ){
            $attr = @substr($method, 3);
        }
        
        if( substr($method, 0, 3) == 'set' ){
            $this->{$attr} = @$params[0];
            return $this;
        }elseif( substr($method, 0, 3) == 'get' ){
            return $this->{$attr};
        }
    }
    
    protected static function geraHashTime( $string = "", $length = 15 )
    {
        $text = $string.time();
        
        return Functions::crypt($text, "PASSHASH", $length);
    }
    
    protected static function geraSlug( $string = "" )
    {
        return Format::strigToUrl($string);
    }

    public static function getSeqTime()
    {
        return str_pad(str_replace(".", "", microtime(true)), 14, "0", STR_PAD_RIGHT);
    }
}
