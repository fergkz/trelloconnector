<?php

namespace System;

use System\Group as Group;
use MVC\Session as Session;
use Core\System\Mail as Mail;
use Core\System\Functions as Functions;

class User extends MyModel
{
    static $daoTable = "hazo_user";
    static $daoPrimary = array('ID' => 'id');
    static $daoCols = array(
        'ID' => 'id',
        'groupID' => 'group_id',
        'login' => 'login',
        'password' => 'password',
        'name' => 'name',
        'status' => 'status',
        'email' => 'email',
        'dateLastAccess' => 'date_last_access',
        'activationCode' => 'activation_code',
        'imageName' => 'image_name',
        'imageContent' => 'image_content',
        'imageSize' => 'image_size',
        'imageType' => 'image_type',
        'imageFilename' => 'image_filename',
    );
    
    public $ID;
    public $groupID;
    public $groupObj;
    public $login;
    public $password;
    public $name;
    public $status;
    public $email;
    public $dateLastAccess;
    public $activationCode;

    public $imageName;
    public $imageContent;
    public $imageSize;
    public $imageType;
    public $imageFilename;

    public $requireActivation = false;
    public $expirationDate = false;

    public function getGroupObj()
    {
        if( !$this->groupObj ){
            $this->groupObj = new Group($this->groupID);
        }
        return $this->groupObj;
    }

    public static function login( $login, $password )
    {
        self::logout();
        $password = md5($password);
        
        $usuExists = self::getList(array(
            'dao.login = ?'    => $login,
            'dao.password = ?' => $password,
            'dao.status = ?' => 'A',
            'gru.status = ?' => 'A'
        ), null, 0, 1);
        
        if( $usuExists['cont_total'] > 0 ){
            $User = $usuExists['rows'][0];
            $User->setTransactionUser();
            return $User;
        }
        return false;
    }
    
    protected function triggerBeforeSave()
    {   
        $this->name = trim($this->name);
        $this->email = trim($this->email);
        $this->login = trim($this->login);
        
        if( !$this->status ){
            $this->raise('O status do usuário deve ser informado');
        }
        
        if( !$this->name ){
            $this->raise('O nome do usuário deve ser informado');
        }
        
        if( !$this->email ){
            $this->raise('O e-mail do usuário deve ser informado');
        }
        
        if( !$this->login ){
            $this->raise('O login do usuário deve ser informado');
        }
        
        if( !$this->password ){
            $this->raise('A senha do usuário deve ser informada');
        }
        
        if( strlen($this->password) < 4 ){
            $this->raise('A senha deve conter no mínimo 4 caracteres');
        }
        
        if( @!preg_match('/^[a-f0-9]{32}$/', $this->password) ){
            $this->password = md5($this->password);
        }
        
    }
    
    protected function triggerAfterSave()
    {
        if( $this->requireActivation ){
            $exists = true;
            
            while( $exists ){
                
                $string = Functions::crypt(time(), 'cad_user', strlen(time())-3);
                $this->activationCode = substr( $string, strlen($string)-10, 10 );
                
                $res = self::getList(array(
                    'dao.activation_code = ?' => $this->activationCode
                ), array('ID'), 0, 1);
                
                if( $res['cont_total'] <= 0 ){
                    break;
                }
            }
            
            $this->requireActivation = false;
            $this->save();
        }
    }
    
    public static function getByActivationCode( $activationCode = null )
    {
        if( !$activationCode ){
            return false;
        }
        
        $usuExists = self::getList(array(
            'dao.activation_code = ?' => $activationCode
        ), null, 0, 1);
        
        if( $usuExists['cont_total'] > 0 ){
            $User = $usuExists['rows'][0];
            return $User;
        }
        
        return false;
    }
    
    public static function logout()
    {
        return Session::destroy();
    }
    
    public function setTransactionUser()
    {
        self::logout();
        $this->getGroupObj();
        Session::set("transaction_user", $this);
        $this->dateLastAccess = date("Y-m-d H:i:s");
        $this->save();
        return $this;
    }
    
    public static function online()
    {
        return Session::get("transaction_user");
    }
    
    public function enviaEmailAtivarNovo()
    {
        $filename = "modelo-emails/usuario-cadastro-novo.twig";
        
        $render['email'] = $this->email;
        $render['name']  = $this->name;
        $render['code']  = $this->activationCode;
        $render['date']  = date('d/m/Y H:i');
        
        $Mail = new Mail();
        $Mail->setAddress($this->email, $this->name);
        $Mail->setMessageTemplate($filename, $render);
        $Mail->setSubject("Cadastro - Diploma na WEB");
        return $Mail->send();
    }
    
    public function enviaEmailAtivadoNovo()
    {
        $filename = "modelo-emails/usuario-cadastrado-novo.twig";
        
        $render['email'] = $this->email;
        $render['name']  = $this->name;
        $render['code']  = $this->activationCode;
        $render['date']  = date('d/m/Y H:i');
        
        $Mail = new Mail();
        $Mail->setAddress($this->email, $this->name);
        $Mail->setMessageTemplate($filename, $render);
        $Mail->setSubject("Cadastro - Diploma na WEB");
        return $Mail->send();
    }
    
    public static function getList( $whereColumns = array(), $loadAttributes = null, $rowStart = 0, $rowLimit = null, $order = array() )
    {
        $join = "join hazo_group gru on gru.id = dao.group_id";
        return parent::getList($whereColumns, $loadAttributes, $rowStart, $rowLimit, $order, $join);
    }
    
}