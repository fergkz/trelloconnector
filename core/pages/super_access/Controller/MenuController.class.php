<?php

use MVC\Controller as Controller;
use Core\Model\Group as Group;
use Core\System\Module as Module;
use Core\Model\Menu as Menu;

class MenuController extends Controller{
    
    public function indexAction( $groupID = null, $mode = null, $module = null, $submodule = null ){
        
        if( $_POST && $this->change() ){
            _setSuccess("hz_menu_saved");
        }
        
        $render['Group'] = Group::getByID($groupID);
        $render['Mode']  = $mode;
        $render['ModeDescription'] = _getModeDescr($mode);
        $render['Module'] = new Module();
        $render['Module']->setMode($mode)->setModule($module)->load();
        $render['Submodule'] = $render['Module']->getSubmodules($submodule);
        $render['menuList'] = Menu::listByGroup($render['Group']->getID());
        
        $this->view()->display($render);
        
    }
    
    private function change(){
        
        if( @$_POST['addToBranch'] ){
            return Menu::getInstance(@$_POST['menu'])
                 ->setMode(@$_POST['mode'])
                 ->setModule(@$_POST['module'])
                 ->setSubmodule(@$_POST['submodule'])
                 ->setDescription(@$_POST['description'])
                 ->setParent(_coalesce(@$_POST['branch'], null))
                 ->setGroupID(@$_POST['group_id'])
                 ->save();
        }elseif( @$_POST['addEmpty'] ){
            return Menu::getInstance()
                 ->setParent(@$_POST['branch'])
                 ->setDescription(@$_POST['add_empty'])
                 ->setGroupID(@$_POST['group_id'])
                 ->save();
        }elseif( @$_POST['resumoEmpty'] ){
            return Menu::getInstance(@$_POST['branch'])
                 ->setDescription(@$_POST['rename_branch'])
                 ->save();
        }elseif( @$_POST['deleteBranch'] ){
            return Menu::deleteAll(@$_POST['branch']);
        }elseif( @$_POST['moveBranch'] == "UP" ){
            return Menu::getInstance(@$_POST['branch'])->moveUp();
        }elseif( @$_POST['moveBranch'] == "DOWN" ){
            return Menu::getInstance(@$_POST['branch'])->moveDown();
        }
    }

}