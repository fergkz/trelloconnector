<?php

use MVC\Controller as Controller;
use MVC\Twig as Twig;

class IndexController extends Controller{
    
    public function indexAction(){
        $render = array();        
        $Twig = new Twig();
        $Template = $Twig->loadTemplate("index/index.html");
        echo $Template->render($render);
    }
    
}